/*!
 * 
 * 
 * 
 * @author Thuclfc
 * @version 2.0.0
 * Copyright 2019. MIT licensed.
 */$(document).ready(function () {
  var timeleft = 10;
  var downloadTimer = setInterval(function () {
    var html_time1 = "<span>" + timeleft + "</span>";
    var html_time2 = "<span class='animated zoomIn'>" + timeleft + "</span>";
    timeleft -= 1;

    if (timeleft <= 10 && timeleft > 3) {
      $('.time').html('').append(html_time1);
    } else if (timeleft < 5) {
      $('.time').html('').append(html_time2);
    }

    if (timeleft < 0) {
      clearInterval(downloadTimer);
      /*$('.time').html('hết giờ');*/

      $('.btn_next').css('display', 'block');
    }
  }, 1000);
  $('.dropdown-toggle').on('click', function () {
    $('.dropdown-menu').toggleClass('show');
  });
  $(document).mouseup(function (e) {
    var container = $(".dropdown-toggle");

    if (!container.is(e.target) && container.has(e.target).length === 0) {
      $('.dropdown-menu').removeClass('show');
    }
  });
  $(window).on('load', function () {
    $('.loading').fadeOut(500);
  }); //scroll effect

  $.fn.isInViewport = function () {
    var elementTop = $(this).offset().top;
    var elementBottom = elementTop + $(this).outerHeight() - 100;
    var viewportTop = $(window).scrollTop();
    var viewportBottom = viewportTop + $(window).height() - 100;
    return elementBottom > viewportTop && elementTop < viewportBottom;
  };

  $(window).on('resize scroll load', function () {
    $('.fadeup').each(function () {
      if ($(this).isInViewport()) {
        $(this).addClass('fadeInUp').css({
          'opacity': '1',
          'visibility': 'visible'
        });
      }
    });
    $('.fadein').each(function () {
      if ($(this).isInViewport()) {
        $(this).addClass('fadeIn').css({
          'opacity': '1',
          'visibility': 'visible'
        });
      }
    });
    $('.zoomin').each(function () {
      if ($(this).isInViewport()) {
        $(this).addClass('zoomIn').css({
          'opacity': '1',
          'visibility': 'visible'
        });
      }
    });
    $('.fadeinleft').each(function () {
      if ($(this).isInViewport()) {
        $(this).addClass('fadeInLeft').css({
          'opacity': '1',
          'visibility': 'visible'
        });
      }
    });
    $('.fadeinright').each(function () {
      if ($(this).isInViewport()) {
        $(this).addClass('fadeInRight').css({
          'opacity': '1',
          'visibility': 'visible'
        });
      }
    });
    $('.bouncein').each(function () {
      if ($(this).isInViewport()) {
        $(this).addClass('bounceIn').css({
          'opacity': '1',
          'visibility': 'visible'
        });
      }
    });
    $('.flipinx').each(function () {
      if ($(this).isInViewport()) {
        $(this).addClass('flipInX').css({
          'opacity': '1',
          'visibility': 'visible'
        });
      }
    });
  });
});